$( document ).ready(function() {
    $.getJSON('https://api.ipify.org?format=json', function(data){
        var obj = JSON.stringify({
            ip: data.ip,
            url:window.location.href
        });
        $.post( "/.netlify/functions/telegram", obj);
        $.post( "/.netlify/functions/google", obj);
    });
});

<!-- google -->
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

if (location.hostname === "localhost" || location.hostname === "127.0.0.1"){
    console.log("running on localhost")
}else {
    ga('create', 'UA-687332-11', 'auto');
    ga('send', 'pageview');
}

