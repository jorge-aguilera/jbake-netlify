yieldUnescaped '<!DOCTYPE html>'
html(lang:'en'){

    head {
        include template: "header.tpl"
    }

    body(class:"antialiased", onload:"prettyPrint();") {

        include template: 'menu.tpl'

        div {
          bodyContents()
        }
        newLine()
        include template: 'footer.tpl'


        script(src:"${config.site_contextPath}js/vendor/jquery.js"){}
        newLine()
        script(src:"${config.site_contextPath}js/foundation.min.js"){}
        newLine()
        script(src:"${config.site_contextPath}js/vendor/prettify.js"){}
        newLine()
        script(src:"${config.site_contextPath}js/app.js"){} newLine()
    }
}
newLine()
